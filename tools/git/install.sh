#!/bin/bash

## $0 is the current running program.  
## if there are only 2 fields, there is only 1 forward slash and you are in the current directory
if [ `echo $0|awk -F/ "{print NF}"` != 2 ]; then  
	echo "installer exiting: must run installer from the project directory (./install.sh)"
	exit
fi

## add in the utilities.
for i in `ls -F git*`
do
	lnk_chk=`ls -F /usr/local/bin/$i|awk '{print substr($0,length($0),1)}'`
	if [ $lnk_chk ]; then
		if [ $lnk_chk != "@" ]; then  ## checks for symlink
			echo "installer exiting: file " $i " already exists in /usr/local/bin and is not a symbolic link"
			exit
		fi
		rm /usr/local/bin/$i 2>/dev/null  # quietly remove old links
	fi
	
	ln -s `pwd`/$i /usr/local/bin/$i

done
