#!/bin/bash

source ./test_utils.sh

response=`git tag-push-new-build`

# confirm that exit response contained the "exiting" status and indicated no repo
check=`echo $response|grep -i exiting|grep -i "no git repo"|wc -l`
if [ $check -gt 0 ]; then
	exit 0
else
	exit 100
fi