#!/bin/bash

#setup
function create_test_repo1 {
	cd tmp
		mkdir repo1
		cd repo1
			echo "test file for test repo" > tr.txt
			git init
			git add .
			git commit -m "test: 1st commit"
		cd ..
	cd ..
}

function create_test_origin {
	## dependent on create_test_repo1
	cd tmp
		mkdir remote
		cd remote
			git init --bare
		cd ..
		cd repo1		
			git remote add origin ../remote/
			git push origin master
		cd ..
	cd ..
}  

function create_init_tag {
	## dependent on create_test_origin
	 cd tmp
	 	cd repo1
	 		git tag-push-first-release
	 		git push --tags
	 	cd ..
	 cd ..	
}


function create_test_repo2_with_master_conflict {
	## dependent on create_test_origin
	## to create another user repo.
	cd tmp
		git clone ./remote repo2
		cd repo2
			echo "another file for test repo" > tr2.txt
			git add .
			git commit -m "test: 2st commit"
			git push origin master
		cd ..
	cd ..
}


#teardown
function remove_test_repo1 {
	cd tmp
		rm -rf repo1
	cd ..

	}

function remove_origin {
	cd tmp
		rm -rf remote
	cd ..
	}

function remove_test_repo2 {
	cd tmp
		rm -rf repo2
	cd ..

	}


# collections

function setup_basic_increment_repo {
	create_test_repo1 > /dev/null 2>&1
	create_test_origin > /dev/null 2>&1
	create_init_tag > /dev/null 2>&1
	}

function remove_basic_increment_repo {
	remove_origin > /dev/null 2>&1
	remove_test_repo1 > /dev/null 2>&1
	}