#!/bin/bash

source ./test_utils.sh
remove_test_repo2 > /dev/null 2>&1
remove_origin > /dev/null 2>&1
remove_test_repo1 > /dev/null 2>&1

# cleanup before starting

# runs all the tests in the current directory and reports status
for i in `ls test_git*.sh`
do 
	echo "-------------------"
	echo "running test for "$i
	`./$i`
	if [ $? -ne 0 ]; then
		echo $i " :FAILED!!!!!!!!!!"
	else
		echo $i " :passed"
	fi
done