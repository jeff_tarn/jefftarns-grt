#!/bin/bash
source ./test_utils.sh

# init
setup_basic_increment_repo

# setup
cd tmp/repo1 
echo "test_git_tag_patch.sh" > build.txt
git add . > /dev/null 2>&1
git commit -m "test patch" > /dev/null 2>&1
git tag-push-new-patch > /dev/null 2>&1
remote_tag=`git ls-remote --tags|sed 's/\^{}//'|awk '{print $2}'|sort -u|tail -1|awk -F/ '{print $NF}'`
cd ../..

# teardown
remove_basic_increment_repo

# test
expected_tag="v0.0.1.0000"

if [ $expected_tag = $remote_tag ]; then
	exit 0
else
	exit 100
fi
