#!/bin/bash

source ./test_utils.sh

# init
create_test_repo1 > /dev/null 2>&1
create_test_origin > /dev/null 2>&1
create_init_tag > /dev/null 2>&1
create_test_repo2_with_master_conflict > /dev/null 2>&1

# setup

cd tmp/repo1 
echo "test_git_tag_wo_merge.sh" > build.txt
git add . > /dev/null 2>&1
git commit -m "test wo merge" > /dev/null 2>&1
response=`git tag-push-new-build`
cd ../..


# teardown
remove_test_repo2 > /dev/null 2>&1
remove_origin > /dev/null 2>&1
remove_test_repo1 > /dev/null 2>&1

# confirm that exit response contained the "exiting" status and indicated the need to merge
check=`echo $response|grep -i exiting|grep -i merge|wc -l`
if [ $check -gt 0 ]; then
	exit 0
else
	exit 100
fi