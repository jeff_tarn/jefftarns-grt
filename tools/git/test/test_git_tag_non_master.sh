#!/bin/bash

source ./test_utils.sh

# init
create_test_repo1 > /dev/null 2>&1
create_test_origin > /dev/null 2>&1
create_init_tag > /dev/null 2>&1

# setup

cd tmp/repo1 
git branch non_master
git checkout non_master
echo "test_git_tag_non_master.sh" > build.txt
git add . > /dev/null 2>&1
git commit -m "test non master" > /dev/null 2>&1
response=`git tag-push-new-major`
cd ../..


# teardown
remove_origin > /dev/null 2>&1
remove_test_repo1 > /dev/null 2>&1

# confirm that exit response contained the "exiting" status and indicated the need to be on master
check=`echo $response|grep -i exiting|grep -i master|wc -l`
if [ $check -gt 0 ]; then
	exit 0
else
	exit 100
fi