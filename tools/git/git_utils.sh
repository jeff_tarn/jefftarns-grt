#!/bin/bash

function f_check_repo_loc {
		# check if there is a git repo here
		ls -ad .git > /dev/null 2>&1
		if [ $? -gt 0 ]; then
			echo "Exiting: no git repo found"
			exit 1
		fi
  }

function f_check_branch_master {
		# git pull
		current_branch=`git status -sb|head -1|awk -F" " '{print $NF}'`
		if [ $current_branch != "master" ]; then
			echo "Exiting: you can only tag the master branch"
			exit 1
		fi
  }

function f_check_current_with_origin {
		# get origin
		git fetch origin

		# See if there are any incoming changes
		changes=`git log HEAD..origin/master --oneline|wc -l`
		if [ $changes -gt 0 ]; then
			echo "Exiting: need to merge from origin before tagging"
			exit 1
		fi
	}
